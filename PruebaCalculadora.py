'''
Created on 12 mar. 2021

@author: Ruby
'''
import tkinter as tk
from tkinter import messagebox as msg
from cgitb import text
import math

ventana = tk.Tk()
ventana.title("Calculadora")
ventana.geometry('200x350')

caja = tk.Entry(ventana)
caja.place(x=0, y=0, width=200, height=50)

guardar = 0
op = 0

n1 = 0
n2 = 0

def btn0():
    caja.insert(tk.INSERT, "0")
    
def btn1():
    caja.insert(tk.INSERT, "1")
    
def btn2():
    caja.insert(tk.INSERT, "2")
    
def btn3():
    caja.insert(tk.INSERT, "3")
    
def btn4():
    caja.insert(tk.INSERT, "4")
    
def btn5():
    caja.insert(tk.INSERT, "5")
    
def btn6():
    caja.insert(tk.INSERT, "6")
    
def btn7():
    caja.insert(tk.INSERT, "7")

def btn8():
    caja.insert(tk.INSERT, "8")
    
def btn9():
    caja.insert(tk.INSERT, "9")

def btnResiduo():
    global op
    global guardar
    guardar = caja.get()
    caja.delete(0, 'end')
    op = 1
    
def btnRaiz():
    if(caja.get()!=""):
        num = float(caja.get())
        if(num > 0):
            num = math.sqrt(num)
            caja.delete(0, 'end')
            caja.insert(0, str(num))
        else:
            caja.delete(0,'end')
            caja.insert(0,"No se puede con numeros negativos")
            
def btnPotencia():
    if(caja.get()!=""):
        num = float(caja.get())
        num = math.pow(num, 2)
        caja.delete(0, 'end')
        caja.insert(0, str(num))
    
def btnLn():
    num = 0.0
    if (caja.get()!="0"):
        if (caja.get()=="" or caja.get()=="."):
            num = 0.0
        else:
            num = float(caja.get())  
        caja.delete(0,'end')
        if (num!=0):
            num = 1/num
            caja.insert(0,str(num))
        else:
            caja.insert(0,"Ingresa un numero")
    else:
        caja.insert(0,"No se puede dividir entre 0")
        
def btnBorrarT():
    global op
    global guardar
    guardar = ""
    caja.delete(0, 'end')
    
def btnBorrar():
    if(caja.get()!=""):
        num = caja.get()[:-1]
        caja.delete(0,'end')
        caja.insert(tk.END,num)

def btnDiv():
    global op
    global guardar
    guardar = caja.get()
    caja.delete(0, 'end')
    op = 2
    
def btnMul():
    global op
    global guardar
    guardar = caja.get()
    caja.delete(0, 'end')
    op = 3
    
def btnRest():
    global op
    global guardar
    guardar = caja.get()
    caja.delete(0, 'end')
    op = 4
    
def btnSum():
    global op
    global guardar
    guardar = caja.get()
    caja.delete(0, 'end')
    op = 5

def btnPunto():
    caja.insert(tk.INSERT,".")

def btnIgual():
    global n1
    global n2
    
    try:
        n1 = float(guardar)
        n2 = float(caja.get())
    except:
        print("error")
    if (op == 1):
        caja.delete(0,'end')
        res = n1%n2
        caja.insert(0, str(res))
    if (op == 2):
        caja.delete(0,'end')
        if(n2==0):
            caja.insert(0, "NO se puede dividir entre 0")
        else:
            num = n1/n2
            caja.insert(0, str(res))
    if (op == 3):
        caja.delete(0,'end')
        res = n1*n2
        caja.insert(0, str(res))
    if (op == 4):
        caja.delete(0,'end')
        res = n1-n2
        caja.insert(0, str(res))
    if (op == 5):
        caja.delete(0,'end')
        res = n1+n2
        caja.insert(0, str(res))
    

bResiduo = tk.Button(ventana, text="%", command=btnResiduo()).place(x=0, y=50, width=50, height=50)
bRaiz = tk.Button(ventana, text="Raiz", command=btnRaiz()).place(x=50, y=50, width=50, height=50)
bPotencia = tk.Button(ventana, text="x^2", command=btnPotencia()).place(x=100, y=50, width=50, height=50)
bLn = tk.Button(ventana, text="1/x", command=btnLn()).place(x=150, y=50, width=50, height=50)

bBorrarT = tk.Button(ventana, text="CE", command=btnBorrarT()).place(x=0, y=100, width=50, height=50)
bBorrar = tk.Button(ventana, text="<[x]", command=btnBorrar()).place(x=50, y=100, width=100, height=50)
bDiv = tk.Button(ventana, text="/", command=btnDiv()).place(x=150, y=100, width=50, height=50)

b7 = tk.Button(ventana, text="7", command=btn7).place(x=0, y=150, width=50, height=50)
b8 = tk.Button(ventana, text="8", command=btn8).place(x=50, y=150, width=50, height=50)
b9 = tk.Button(ventana, text="9", command=btn9).place(x=100, y=150, width=50, height=50)
bx = tk.Button(ventana, text="x").place(x=150, y=150, width=50, height=50)

b4 = tk.Button(ventana, text="4", command=btn4).place(x=0, y=200, width=50, height=50)
b5 = tk.Button(ventana, text="5", command=btn5).place(x=50, y=200, width=50, height=50)
b6 = tk.Button(ventana, text="6", command=btn6).place(x=100, y=200, width=50, height=50)
bRest = tk.Button(ventana, text="-").place(x=150, y=200, width=50, height=50)

b1 = tk.Button(ventana, text="1", command=btn1).place(x=0, y=250, width=50, height=50)
b2 = tk.Button(ventana, text="2", command=btn2).place(x=50, y=250, width=50, height=50)
b3 = tk.Button(ventana, text="3", command=btn3).place(x=100, y=250, width=50, height=50)
bSum = tk.Button(ventana, text="+").place(x=150, y=250, width=50, height=50)

bPunto = tk.Button(ventana, text=".", command=btnPunto()).place(x=0, y=300, width=50, height=50)
b0 = tk.Button(ventana, text="0", command=btn0).place(x=50, y=300, width=50, height=50)
bIgual = tk.Button(ventana, text="=", command=btnIgual()).place(x=100, y=300, width=100, height=50)


ventana.mainloop()
